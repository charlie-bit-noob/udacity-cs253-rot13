months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']
          
def valid_month(month):
  if month:
    capMonth = month.capitalize()
    if capMonth in months:
      return capMonth
    else:
      return None

def valid_year(year):
  if year.isdigit():
    year = int(year)
    if year >= 1900 and year <= 2020:
      return year

def valid_day(day):
  if day.isdigit():
    day = int(day)
    if day > 0 and not day > 31:
      return day

def escape_html(s):
  for (i,o) in (("&", "&amp;"),(">", "&gt;"),("<", "&lt;"),('"', "&quot;")):
    s = s.replace(i,o)
  return s

alph = ['a','b','c','d','e','f','g','h','i','j','k','l','m',
        'n','o','p','q','r','s','t','u','v','w','x','y','z']

def cap_let(rot_tmp=[],rsrv=[]):
  ret = []
  i = 0
  for s in rsrv:
    if (s.isupper()):
      ret.append(rot_tmp[i].upper())
    else:
      ret.append(rot_tmp[i])
    i += 1

  return ret

def rot(unrot):
  resrv = list(unrot)
  unrot = unrot.lower()
  i = -1
  tmp = []
  for let in list(unrot):
    if let in alph:
      i = alph.index(let)
      if (i >= 13):
        tmp.append(alph[(i - 13)])
      elif (i <= 12):
        tmp.append(alph[(i + 13)])
    else:
      tmp.append(let)
      
  return ''.join(cap_let(tmp, resrv))
